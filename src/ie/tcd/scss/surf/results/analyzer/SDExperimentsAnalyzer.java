package ie.tcd.scss.surf.results.analyzer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.commons.math3.stat.descriptive.rank.Max;
import org.apache.commons.math3.stat.descriptive.rank.Median;
import org.apache.commons.math3.stat.descriptive.rank.Min;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import ie.tcd.scss.surf.results.entities.Plan;
import ie.tcd.scss.surf.results.entities.RequestDescription;
import ie.tcd.scss.surf.results.entities.ServiceDescription;
import ie.tcd.scss.surf.results.matchmaker.Matchmaker;
import ie.tcd.scss.surf.results.util.Util;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.UnderlineStyle;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class SDExperimentsAnalyzer {

	private static WritableCellFormat timesBoldUnderline;
	private static WritableCellFormat times;

	public SDExperimentsAnalyzer() {

	}

	/**
	 * 
	 * Reads results data and create the csv files with the data
	 */
	public static void resultsAnalyzer() {



		String path = "../../Results/RawData/";
		//String pathResults = "../../Results/right-service-right-place-results/analyzed/";
		String pathResults = "../../Results/Analyzed/";
		String churnVariable = ""; 
		String approachVariable = ""; 
		String servicesVariable = ""; 
		String hopsLimitVariable="";
		String fileName = "";

		String gatewaysVariable = "number-of-gateways-500"; 
		String providersVariable = "number-of-providers-200";
		String citizensVariable = "number-of-citizens-100";
		String topSimilarGatewaysVariable = "top-similar-gateways-5";

		try {
			for (int churn = 1; churn >=  1; churn--) {
				if (churn == 0)churnVariable = "no-churn"; 
				else churnVariable = "churn";	 
				for (int approach = 3; approach <= 3; approach++) {
					for (int services = 20000; services >= 20000; services = services - 20000) {
						servicesVariable = "number-of-services-"+services; 
						for(int hopsLimit = 5; hopsLimit>=5;hopsLimit = hopsLimit -1){
							hopsLimitVariable  ="hops-limit-"+hopsLimit;
							if (approach == 1) {
								approachVariable = "Location"; 
								String distanceToRegisterServiceVariable = ""; 
								for(int distanceToRegisterService = 100; distanceToRegisterService>=50; distanceToRegisterService= distanceToRegisterService - 10 ){
									distanceToRegisterServiceVariable="distance-register-service-"+distanceToRegisterService;
									String pathToRawData = path + churnVariable + "/" + approachVariable + "/" + servicesVariable + "/" + gatewaysVariable + "/" +  providersVariable + "/" + citizensVariable + "/" + hopsLimitVariable + "/" + distanceToRegisterServiceVariable + "/";
									if(existData(pathToRawData)){
										fileName = approachVariable + "_" + churnVariable + "_" + servicesVariable + "_" + hopsLimitVariable + "_" + distanceToRegisterServiceVariable + ".xls";
										if(!alreadyAnalyzed(pathResults + "/" + fileName)){
											File file = new File(pathResults + "/" + fileName);
											file.createNewFile();
											WorkbookSettings wbSettings = new WorkbookSettings();

											wbSettings.setLocale(new Locale("en", "EN"));

											WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
											workbook.createSheet("ConfigMessages", 0);
											workbook.createSheet("SolvedRequests", 1);
											workbook.createSheet("Requests", 2);

											System.out.println("Analysing: " + pathToRawData + "messages");
											WritableSheet excelSheet = workbook.getSheet(0);
											createLabelConfigMessages(excelSheet, pathToRawData + "messages");
											createContentConfigMessages(excelSheet, pathToRawData + "messages");

											System.out.println("Analysing: " + pathToRawData + "notSolvedRequests");
											excelSheet = workbook.getSheet(1);
											createLabelSolvedRequests(excelSheet, pathToRawData + "requests");
											createContentSolvedRequests(excelSheet, pathToRawData + "requests");

											System.out.println("Analysing: " + pathToRawData + "messages");
											excelSheet = workbook.getSheet(2);
											createLabelRequests(excelSheet, pathToRawData + "requests");
											createContentRequets(excelSheet, pathToRawData + "requests");

											workbook.write();
											workbook.close();
										}
									}
								}
							}
							if (approach == 2) {
								approachVariable = "Domain"; 
								String numberOfDomainsVariable = "";
								for(int numberOfDomains = 5; numberOfDomains>=1; numberOfDomains= numberOfDomains - 1){
									numberOfDomainsVariable="number-domains-"+numberOfDomains;
									String pathToRawData = path + churnVariable + "/" + approachVariable + "/" + servicesVariable + "/" + gatewaysVariable + "/" +  providersVariable + "/" + citizensVariable + "/" + hopsLimitVariable + "/" + numberOfDomainsVariable + "/";
									if(existData(pathToRawData)){
										fileName = approachVariable + "_" + churnVariable + "_" + servicesVariable + "_" + hopsLimitVariable + "_" + numberOfDomainsVariable + ".xls";
										if(!alreadyAnalyzed(pathResults + "/" + fileName)){
											File file = new File(pathResults + "/" + fileName);
											file.createNewFile();
											WorkbookSettings wbSettings = new WorkbookSettings();

											wbSettings.setLocale(new Locale("en", "EN"));

											WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
											workbook.createSheet("ConfigMessages", 0);
											workbook.createSheet("SolvedRequests", 1);
											workbook.createSheet("Requests", 2);

											System.out.println("Analysing: " + pathToRawData + "messages");
											WritableSheet excelSheet = workbook.getSheet(0);
											createLabelConfigMessages(excelSheet, pathToRawData + "messages");
											createContentConfigMessages(excelSheet, pathToRawData + "messages");

											System.out.println("Analysing: " + pathToRawData + "notSolvedRequests");
											excelSheet = workbook.getSheet(1);
											createLabelSolvedRequests(excelSheet, pathToRawData + "requests");
											createContentSolvedRequests(excelSheet, pathToRawData + "requests");

											System.out.println("Analysing: " + pathToRawData + "requests");
											excelSheet = workbook.getSheet(2);
											createLabelRequests(excelSheet, pathToRawData + "requests");
											createContentRequets(excelSheet, pathToRawData + "requests");

											workbook.write();
											workbook.close();
										}
									}
								}
							}
							if (approach == 3) {
								approachVariable = "SmartSpace";
								String distanceToRecogniseClosePlacesVariable = ""; 
								for(int distanceToRecogniseClosePlaces = 100; distanceToRecogniseClosePlaces>=50; distanceToRecogniseClosePlaces= distanceToRecogniseClosePlaces - 10 ){
									distanceToRecogniseClosePlacesVariable="distance-close-places-"+distanceToRecogniseClosePlaces;
									String pathToRawData = path + churnVariable + "/" + approachVariable + "/" + servicesVariable + "/" + gatewaysVariable + "/" +  providersVariable + "/" + citizensVariable + "/" + hopsLimitVariable + "/" + distanceToRecogniseClosePlacesVariable + "/" + topSimilarGatewaysVariable + "/"; 
									if(existData(pathToRawData)){
										fileName = approachVariable + "_" + churnVariable + "_" + servicesVariable + "_" + hopsLimitVariable + "_" + distanceToRecogniseClosePlacesVariable + ".xls";
										if(!alreadyAnalyzed(pathResults + "/" + fileName)){
											File file = new File(pathResults + "/" + fileName);
											file.createNewFile();
											WorkbookSettings wbSettings = new WorkbookSettings();
											wbSettings.setLocale(new Locale("en", "EN"));

											WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
											workbook.createSheet("ConfigMessages", 0);
											workbook.createSheet("SolvedRequests", 1);
											workbook.createSheet("Requests", 2);

											System.out.println("Analysing: " + pathToRawData + "messages");
											WritableSheet excelSheet = workbook.getSheet(0);
											createLabelConfigMessages(excelSheet, pathToRawData + "messages");
											createContentConfigMessages(excelSheet, pathToRawData + "messages");

											System.out.println("Analysing: " + pathToRawData + "notSolvedRequests");
											excelSheet = workbook.getSheet(1);
											createLabelSolvedRequests(excelSheet, pathToRawData + "requests");
											createContentSolvedRequests(excelSheet, pathToRawData + "requests");

											System.out.println("Analysing: " + pathToRawData + "requests");
											excelSheet = workbook.getSheet(2);
											createLabelRequests(excelSheet, pathToRawData + "requests");
											createContentRequets(excelSheet, pathToRawData + "requests");


											workbook.write();
											workbook.close();
										}
									}
								}
							}
							if (approach == 4) {
								approachVariable = "SmartSpaceResponse";
								String distanceToRecogniseClosePlacesVariable = ""; 
								for(int distanceToRecogniseClosePlaces = 100; distanceToRecogniseClosePlaces>=50; distanceToRecogniseClosePlaces= distanceToRecogniseClosePlaces - 10 ){
									distanceToRecogniseClosePlacesVariable="distance-close-places-"+distanceToRecogniseClosePlaces;
									String pathToRawData = path + churnVariable + "/" + approachVariable + "/" + servicesVariable + "/" + gatewaysVariable + "/" +  providersVariable + "/" + citizensVariable + "/" + hopsLimitVariable + "/" + distanceToRecogniseClosePlacesVariable + "/" + topSimilarGatewaysVariable + "/";;
									if(existData(pathToRawData)){
										fileName = approachVariable + "_" + churnVariable + "_" + servicesVariable + "_" + hopsLimitVariable + "_" + distanceToRecogniseClosePlacesVariable + ".xls";
										if(!alreadyAnalyzed(pathResults + "/" + fileName)){
											File file = new File(pathResults + "/" + fileName);
											file.createNewFile();
											WorkbookSettings wbSettings = new WorkbookSettings();
											wbSettings.setLocale(new Locale("en", "EN"));

											WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
											workbook.createSheet("ConfigMessages", 0);
											workbook.createSheet("SolvedRequests", 1);
											workbook.createSheet("Requests", 2);

											System.out.println("Analysing: " + pathToRawData + "messages");
											WritableSheet excelSheet = workbook.getSheet(0);
											createLabelConfigMessages(excelSheet, pathToRawData + "messages");
											createContentConfigMessages(excelSheet, pathToRawData + "messages");

											System.out.println("Analysing: " + pathToRawData + "notSolvedRequests");
											excelSheet = workbook.getSheet(1);
											createLabelSolvedRequests(excelSheet, pathToRawData + "requests");
											createContentSolvedRequests(excelSheet, pathToRawData + "requests");

											System.out.println("Analysing: " + pathToRawData + "requests");
											excelSheet = workbook.getSheet(2);
											createLabelRequests(excelSheet, pathToRawData + "requests");
											createContentRequets(excelSheet, pathToRawData + "requests");								        

											workbook.write();
											workbook.close();
										}
									}
								}
							}
						}
					}
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private static boolean alreadyAnalyzed(String pathname) {
		File f = new File(pathname);
		if(f.exists()){
			return true;
		}
		else return false;
	}

	private static boolean existData(String pathToRawData) {
		File folderResults = new File(pathToRawData);
		System.out.println("Analysing: " + pathToRawData + "messages");
		File[] folders = folderResults.listFiles();
		if(folders!=null){
			System.out.println("Data Available: " + pathToRawData + "messages");
			if(folders.length>=3){
				if(folders.length==4){
					for(int i=0;i<folders.length;i++){
						if(folders[i].getName().contains("services")){
							System.out.println("Deleting services: " + pathToRawData);
							File[] files = folders[i].listFiles();
							for(int j=0; j<files.length; j++){
								files[j].delete();
							}
							folders[i].delete();
						}
					}
				}
				return true;
			}else{
				return false;
			}
		}else{
			System.out.println("No Data: " + pathToRawData);
			return false;
		}
	}

	private static void createLabelConfigMessages(WritableSheet excelSheet, String string)
			throws WriteException {
		// Lets create a times font
		WritableFont times10pt = new WritableFont(WritableFont.TIMES, 10);
		// Define the cell format
		times = new WritableCellFormat(times10pt);
		// Lets automatically wrap the cells
		times.setWrap(true);

		// create create a bold font with unterlines
		WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD, false,UnderlineStyle.NO_UNDERLINE);
		timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
		// Lets automatically wrap the cells
		timesBoldUnderline.setWrap(true);

		CellView cv = new CellView();
		cv.setAutosize(true);
		// Write a few headers
		addCaption(excelSheet, 0, 0, "Gateway");
		addCaption(excelSheet, 1, 0, "Configuration Messages");

	}

	private static void createLabelSolvedRequests(WritableSheet excelSheet, String string) throws WriteException {
		// Lets create a times font
		WritableFont times10pt = new WritableFont(WritableFont.TIMES, 10);
		// Define the cell format
		times = new WritableCellFormat(times10pt);
		// Lets automatically wrap the cells
		times.setWrap(true);

		// create create a bold font with unterlines
		WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD, false,UnderlineStyle.NO_UNDERLINE);
		timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
		// Lets automatically wrap the cells
		timesBoldUnderline.setWrap(true);

		CellView cv = new CellView();
		cv.setAutosize(true);
		// Write a few headers
		addCaption(excelSheet, 0, 0, "Consumer");
		addCaption(excelSheet, 1, 0, "Total Requests");
		addCaption(excelSheet, 2, 0, "Solved Requests");
		addCaption(excelSheet, 3, 0, "Not Solved Requests");
	}

	private static void createLabelRequests(WritableSheet excelSheet, String string) throws WriteException {
		// Lets create a times font
		WritableFont times10pt = new WritableFont(WritableFont.TIMES, 10);
		// Define the cell format
		times = new WritableCellFormat(times10pt);
		// Lets automatically wrap the cells
		times.setWrap(true);

		// create create a bold font with unterlines
		WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD, false,UnderlineStyle.NO_UNDERLINE);
		timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
		// Lets automatically wrap the cells
		timesBoldUnderline.setWrap(true);

		CellView cv = new CellView();
		cv.setAutosize(true);
		// Write a few headers
		addCaption(excelSheet, 0, 0, "Request");
		addCaption(excelSheet, 1, 0, "Hops");
		addCaption(excelSheet, 2, 0, "Precision");
		addCaption(excelSheet, 3, 0, "Recall");
		addCaption(excelSheet, 4, 0, "Response Time");
	}

	private static void createContentConfigMessages(WritableSheet excelSheet, String path) throws WriteException,
	RowsExceededException{
		try{
			File folderResults = new File(path);
			JSONParser parser = new JSONParser();
			File[] messages = folderResults.listFiles();
			int row = 1;
			for (int j = 0; j < messages.length; j++) {
				FileReader reader = new FileReader(messages[j].getAbsolutePath());
				Object obj = parser.parse(reader);
				JSONObject jsonObject = (JSONObject) obj;
				reader.close();
				String gateway = messages[j].getName().replace("configMessages_", "").replace(".json","");
				addLabel(excelSheet,0,(j+1),gateway);
				Long configMessages = (Long) jsonObject.get("configMessages");
				addNumber(excelSheet, 1, (j+1), configMessages);
				row ++;
			}

			addLabel(excelSheet,0,row,"MIN");
			StringBuffer buf = new StringBuffer();
			buf.append("QUARTILE(B2:B"+row+",0)");
			Formula f = new Formula(1, row, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+1,"Q1");
			buf = new StringBuffer();
			buf.append("QUARTILE(B2:B"+row+",1)");
			f = new Formula(1, row+1, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+2,"MEDIAN");
			buf = new StringBuffer();
			buf.append("QUARTILE(B2:B"+row+",2)");
			f = new Formula(1, row+2, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+3,"Q3");
			buf = new StringBuffer();
			buf.append("QUARTILE(B2:B"+row+",3)");
			f = new Formula(1, row+3, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+4,"MAX");
			buf = new StringBuffer();
			buf.append("QUARTILE(B2:B"+row+",4)");
			f = new Formula(1, row+4, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+5,"AVERAGE");
			buf = new StringBuffer();
			buf.append("AVERAGE(B2:B"+row+")");
			f = new Formula(1, row+5, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+6,"SUM");
			buf = new StringBuffer();
			buf.append("SUM(B2:B"+row+")");
			f = new Formula(1, row+6, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+7,"PERCENTAGE");
			buf = new StringBuffer();
			buf.append("B"+(row+6)+"/"+"B"+(row+6));
			f = new Formula(1, row+7, buf.toString());
			excelSheet.addCell(f);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private static void createContentSolvedRequests(WritableSheet excelSheet, String path) {
		try{
			File folderConsumers = new File(path);
			JSONParser parser = new JSONParser();
			File[] consumers = folderConsumers.listFiles();
			int row = 1;
			for (int j = 0; j < consumers.length; j++) {
				String n = consumers[j].getName().replace("Consumer_", "");
				FileReader reader = new FileReader(consumers[j].getAbsolutePath()+"/notSolvedRequests_"+n+".json");
				Object obj = parser.parse(reader);
				JSONObject jsonObject = (JSONObject) obj;
				reader.close();
				String consumer = consumers[j].getName();
				addLabel(excelSheet,0,(j+1),consumer);
				Long total = (Long) jsonObject.get("total");
				addNumber(excelSheet, 1, (j+1), total);
				Long solved = (Long) jsonObject.get("solved");
				addNumber(excelSheet, 2, (j+1), solved);
				Long notSolved = (Long) jsonObject.get("notSolved");
				addNumber(excelSheet, 3, (j+1), notSolved);
				row ++;
			}

			addLabel(excelSheet,0,row,"MIN");
			StringBuffer buf = new StringBuffer();
			buf.append("QUARTILE(B2:B"+row+",0)");
			Formula f = new Formula(1, row, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(C2:C"+row+",0)");
			f = new Formula(2, row, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(D2:D"+row+",0)");
			f = new Formula(3, row, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+1,"Q1");
			buf = new StringBuffer();
			buf.append("QUARTILE(B2:B"+row+",1)");
			f = new Formula(1, row+1, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(C2:C"+row+",1)");
			f = new Formula(2, row+1, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(D2:D"+row+",1)");
			f = new Formula(3, row+1, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+2,"MEDIAN");
			buf = new StringBuffer();
			buf.append("QUARTILE(B2:B"+row+",2)");
			f = new Formula(1, row+2, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(C2:C"+row+",2)");
			f = new Formula(2, row+2, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(D2:D"+row+",2)");
			f = new Formula(3, row+2, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+3,"Q3");
			buf = new StringBuffer();
			buf.append("QUARTILE(B2:B"+row+",3)");
			f = new Formula(1, row+3, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(C2:C"+row+",3)");
			f = new Formula(2, row+3, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(D2:D"+row+",3)");
			f = new Formula(3, row+3, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+4,"MAX");
			buf = new StringBuffer();
			buf.append("QUARTILE(B2:B"+row+",4)");
			f = new Formula(1, row+4, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(C2:C"+row+",4)");
			f = new Formula(2, row+4, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(D2:D"+row+",4)");
			f = new Formula(3, row+4, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+5,"AVERAGE");
			buf = new StringBuffer();
			buf.append("AVERAGE(B2:B"+row+")");
			f = new Formula(1, row+5, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("AVERAGE(C2:C"+row+")");
			f = new Formula(2, row+5, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("AVERAGE(D2:D"+row+")");
			f = new Formula(3, row+5, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+6,"SUM");
			buf = new StringBuffer();
			buf.append("SUM(B2:B"+row+")");
			f = new Formula(1, row+6, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("SUM(C2:C"+row+")");
			f = new Formula(2, row+6, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("SUM(D2:D"+row+")");
			f = new Formula(3, row+6, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+7,"PERCENTAGE");
			buf = new StringBuffer();
			buf.append("B"+(row+6)+"/"+"B"+(row+6));
			f = new Formula(1, row+7, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("C"+(row+6)+"/"+"B"+(row+6));
			f = new Formula(2, row+7, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("D"+(row+6)+"/"+"B"+(row+6));
			f = new Formula(3, row+7, buf.toString());
			excelSheet.addCell(f);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	private static void createContentSolvedRequests2(WritableSheet excelSheet, String path) {
		try{
			File folderConsumers = new File(path);
			JSONParser parser = new JSONParser();
			File[] consumers = folderConsumers.listFiles();
			int row = 1;
			for (int j = 0; j < consumers.length; j++) {
				int solved= 0;
				int notSolved= 0;
				int total= 0;
				File[] requests = consumers[j].listFiles();
				for(int i = 0; i < requests.length; i++){
					total++; 
					FileReader reader = new FileReader(requests[i].getAbsolutePath());
					Object obj = parser.parse(reader);
					JSONObject jsonObject = (JSONObject) obj;
					JSONArray responses = new JSONArray();
					if(jsonObject.containsKey("responses"))
						responses = (JSONArray)jsonObject.get("responses");
					reader.close();
					if(responses.size()>0){
						solved++;
					}else{
						notSolved++;
					}
				}
				String consumer = consumers[j].getName();
				addLabel(excelSheet,0,(j+1),consumer);
				addNumber(excelSheet, 1, (j+1), total);
				addNumber(excelSheet, 2, (j+1), solved);
				addNumber(excelSheet, 3, (j+1), notSolved);
				row++;
			}
			addLabel(excelSheet,0,row,"MIN");
			StringBuffer buf = new StringBuffer();
			buf.append("QUARTILE(B2:B"+row+",0)");
			Formula f = new Formula(1, row, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(C2:C"+row+",0)");
			f = new Formula(2, row, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(D2:D"+row+",0)");
			f = new Formula(3, row, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+1,"Q1");
			buf = new StringBuffer();
			buf.append("QUARTILE(B2:B"+row+",1)");
			f = new Formula(1, row+1, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(C2:C"+row+",1)");
			f = new Formula(2, row+1, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(D2:D"+row+",1)");
			f = new Formula(3, row+1, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+2,"MEDIAN");
			buf = new StringBuffer();
			buf.append("QUARTILE(B2:B"+row+",2)");
			f = new Formula(1, row+2, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(C2:C"+row+",2)");
			f = new Formula(2, row+2, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(D2:D"+row+",2)");
			f = new Formula(3, row+2, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+3,"Q3");
			buf = new StringBuffer();
			buf.append("QUARTILE(B2:B"+row+",3)");
			f = new Formula(1, row+3, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(C2:C"+row+",3)");
			f = new Formula(2, row+3, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(D2:D"+row+",3)");
			f = new Formula(3, row+3, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+4,"MAX");
			buf = new StringBuffer();
			buf.append("QUARTILE(B2:B"+row+",4)");
			f = new Formula(1, row+4, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(C2:C"+row+",4)");
			f = new Formula(2, row+4, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("QUARTILE(D2:D"+row+",4)");
			f = new Formula(3, row+4, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+5,"AVERAGE");
			buf = new StringBuffer();
			buf.append("AVERAGE(B2:B"+row+")");
			f = new Formula(1, row+5, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("AVERAGE(C2:C"+row+")");
			f = new Formula(2, row+5, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("AVERAGE(D2:D"+row+")");
			f = new Formula(3, row+5, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+6,"SUM");
			buf = new StringBuffer();
			buf.append("SUM(B2:B"+row+")");
			f = new Formula(1, row+6, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("SUM(C2:C"+row+")");
			f = new Formula(2, row+6, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("SUM(D2:D"+row+")");
			f = new Formula(3, row+6, buf.toString());
			excelSheet.addCell(f);

			addLabel(excelSheet,0,row+7,"PERCENTAGE");
			buf = new StringBuffer();
			buf.append("B"+(row+6)+"/"+"B"+(row+6));
			f = new Formula(1, row+7, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("C"+(row+6)+"/"+"B"+(row+6));
			f = new Formula(2, row+7, buf.toString());
			excelSheet.addCell(f);
			buf = new StringBuffer();
			buf.append("D"+(row+6)+"/"+"B"+(row+6));
			f = new Formula(3, row+7, buf.toString());
			excelSheet.addCell(f);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	private static void createContentRequets(WritableSheet excelSheet, String path) {
		try{
			File folderConsumers = new File(path);
			JSONParser parser = new JSONParser();
			File[] consumers = folderConsumers.listFiles();
			int row = 1;
			for (int j = 0; j < consumers.length; j++) {
				int solved= 0;
				int notSolved= 0;
				int total= 0;
				File[] requests = consumers[j].listFiles();
				for(int i = 0; i < requests.length; i++){
					total++; 
					FileReader reader = new FileReader(requests[i].getAbsolutePath());
					Object obj = parser.parse(reader);
					JSONObject jsonObject = (JSONObject) obj;
					JSONArray responses = new JSONArray();
					if(jsonObject.containsKey("responses"))
						responses = (JSONArray)jsonObject.get("responses");
					reader.close();
					if(responses.size()>0){
						solved++;
						String n = requests[i].getName().replace(".json", "");
						addLabel(excelSheet,0,row,n);
						ArrayList<ServiceDescription> serviceDescriptions = loadServices("../../Data/right-place-dataset/experiments-dataset/IoT-services");
						double responseTime = 0.0;
						long hops = 0;
						double precision = 0.0;
						double recall = 0.0;
						RequestDescription request = Util.parseJsonToRequestDescription((JSONObject) jsonObject.get("request"));
						Matchmaker m = new Matchmaker();
						ArrayList<Plan> expectedPlans = m.backwardPlanning(serviceDescriptions,request, new ArrayList<Plan>());
						ArrayList<Plan> checkedPlans = checkPlans(expectedPlans, request);


						String accuracy = calculateAccuracy(checkedPlans, responses);
						precision = Double.parseDouble(accuracy.split("/")[0]);
						addNumber(excelSheet, 2, row, precision);
						recall = Double.parseDouble(accuracy.split("/")[1]);
						addNumber(excelSheet, 3, row, recall);

						for (int x = 0; x < responses.size(); x++) {
							JSONObject plan = (JSONObject) responses.get(x);
							long h = (long) plan.get("hops");
							hops = hops + h;
							double rt = (long) plan.get("time");
							responseTime = responseTime + rt;
						}

						hops = hops / responses.size();
						if(hops>5){
							System.out.println("aqui");
						}
						addNumber(excelSheet, 1, row, hops);
						responseTime = responseTime / responses.size();
						addNumber(excelSheet, 4, row, responseTime);
						row ++;
					}else{
						notSolved++;
					}
				}
				if(row>1){
					addLabel(excelSheet,0,row,"MIN");
					StringBuffer buf = new StringBuffer();
					buf.append("QUARTILE(B2:B"+row+",0)");
					Formula f = new Formula(1, row, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("QUARTILE(C2:C"+row+",0)");
					f = new Formula(2, row, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("QUARTILE(D2:D"+row+",0)");
					f = new Formula(3, row, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("QUARTILE(E2:E"+row+",0)");
					f = new Formula(4, row, buf.toString());
					excelSheet.addCell(f);

					addLabel(excelSheet,0,row+1,"Q1");
					buf = new StringBuffer();
					buf.append("QUARTILE(B2:B"+row+",1)");
					f = new Formula(1, row+1, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("QUARTILE(C2:C"+row+",1)");
					f = new Formula(2, row+1, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("QUARTILE(D2:D"+row+",1)");
					f = new Formula(3, row+1, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("QUARTILE(E2:E"+row+",1)");
					f = new Formula(4, row+1, buf.toString());
					excelSheet.addCell(f);

					addLabel(excelSheet,0,row+2,"MEDIAN");
					buf = new StringBuffer();
					buf.append("QUARTILE(B2:B"+row+",2)");
					f = new Formula(1, row+2, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("QUARTILE(C2:C"+row+",2)");
					f = new Formula(2, row+2, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("QUARTILE(D2:D"+row+",2)");
					f = new Formula(3, row+2, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("QUARTILE(E2:E"+row+",2)");
					f = new Formula(4, row+2, buf.toString());
					excelSheet.addCell(f);

					addLabel(excelSheet,0,row+3,"Q3");
					buf = new StringBuffer();
					buf.append("QUARTILE(B2:B"+row+",3)");
					f = new Formula(1, row+3, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("QUARTILE(C2:C"+row+",3)");
					f = new Formula(2, row+3, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("QUARTILE(D2:D"+row+",3)");
					f = new Formula(3, row+3, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("QUARTILE(E2:E"+row+",3)");
					f = new Formula(4, row+3, buf.toString());
					excelSheet.addCell(f);

					addLabel(excelSheet,0,row+4,"MAX");
					buf = new StringBuffer();
					buf.append("QUARTILE(B2:B"+row+",4)");
					f = new Formula(1, row+4, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("QUARTILE(C2:C"+row+",4)");
					f = new Formula(2, row+4, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("QUARTILE(D2:D"+row+",4)");
					f = new Formula(3, row+4, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("QUARTILE(E2:E"+row+",4)");
					f = new Formula(4, row+4, buf.toString());
					excelSheet.addCell(f);

					addLabel(excelSheet,0,row+5,"AVERAGE");
					buf = new StringBuffer();
					buf.append("AVERAGE(B2:B"+row+")");
					f = new Formula(1, row+5, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("AVERAGE(C2:C"+row+")");
					f = new Formula(2, row+5, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("AVERAGE(D2:D"+row+")");
					f = new Formula(3, row+5, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("AVERAGE(E2:E"+row+")");
					f = new Formula(4, row+5, buf.toString());
					excelSheet.addCell(f);

					addLabel(excelSheet,0,row+6,"SUM");
					buf = new StringBuffer();
					buf.append("SUM(B2:B"+row+")");
					f = new Formula(1, row+6, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("SUM(C2:C"+row+")");
					f = new Formula(2, row+6, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("SUM(D2:D"+row+")");
					f = new Formula(3, row+6, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("SUM(E2:E"+row+")");
					f = new Formula(4, row+6, buf.toString());
					excelSheet.addCell(f);

					addLabel(excelSheet,0,row+7,"PERCENTAGE");
					buf = new StringBuffer();
					buf.append("B"+(row+6)+"/"+"B"+(row+6));
					f = new Formula(1, row+7, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("C"+(row+6)+"/"+"B"+(row+6));
					f = new Formula(2, row+7, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("D"+(row+6)+"/"+"B"+(row+6));
					f = new Formula(3, row+7, buf.toString());
					excelSheet.addCell(f);
					buf = new StringBuffer();
					buf.append("E"+(row+6)+"/"+"B"+(row+6));
					f = new Formula(4, row+7, buf.toString());
					excelSheet.addCell(f);
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private static void addCaption(WritableSheet excelSheet, int column, int row, String s) throws RowsExceededException, WriteException {
		Label label;
		label = new Label(column, row, s);
		excelSheet.addCell(label);

	}

	private static void addNumber(WritableSheet excelSheet, int column, int row,Long n) throws WriteException, RowsExceededException {
		Number number;
		number = new Number(column, row, n, times);
		excelSheet.addCell(number);
	}

	private static void addNumber(WritableSheet excelSheet, int column, int row, double n) throws WriteException, RowsExceededException {
		Number number;
		number = new Number(column, row, n, times);
		excelSheet.addCell(number);
	}

	private static void addLabel(WritableSheet excelSheet, int column, int row, String s)
			throws WriteException, RowsExceededException {
		Label label;
		label = new Label(column, row, s, times);
		excelSheet.addCell(label);
	}



	/**
	 * 
	 * Check if the retrieved services contains the request domains
	 */
	private static ArrayList<Plan> checkPlans(ArrayList<Plan> expectedPlans, RequestDescription request) {
		ArrayList<Plan> plans = new ArrayList<Plan>();
		for (int i = 0; i < expectedPlans.size(); i++) {
			Plan plan = expectedPlans.get(i);
			boolean add = false;
			if(plan.getState()==1){
				ArrayList<String> tempDomains = new ArrayList<String>();
				for(int x =0;x<plan.getDomains().size();x++){
					tempDomains.add(plan.getDomains().get(x).getUrl());
				}
				for(int j = 0; j < request.getDomains().size(); j++){
					if(tempDomains.contains(request.getDomains().get(j).getUrl())){
						add=true;
						break;
					}
				}
			}
			if(add) plans.add(plan);
		}
		return plans;
	}

	/**
	 * 
	 * Calculates search accuracy (i.e., precision and recall)
	 */
	private static String calculateAccuracy(ArrayList<Plan> expectedPlans, JSONArray responses) {
		String res = "";
		JSONObject expectedResponses = Util.parsePlanListtoJSON(expectedPlans);
		JSONArray expectedArray = (JSONArray) expectedResponses.get("plans");
		ArrayList<String> expected = new ArrayList<String>();
		for (int i = 0; i < expectedArray.size(); i++) {
			JSONObject expectedResponse = (JSONObject) expectedArray.get(i);
			expected.add(getServicesNames(expectedResponse));
		}

		ArrayList<String> retrieved = new ArrayList<String>();
		for (int i = 0; i < responses.size(); i++) {
			JSONObject retrievedResponses = (JSONObject) responses.get(i);
			JSONArray retrievedArray = (JSONArray) retrievedResponses.get("plans");
			for (int j = 0; j < retrievedArray.size(); j++) {
				JSONObject retrievedResponse = (JSONObject) retrievedArray.get(j);
				retrieved.add(getServicesNames(retrievedResponse));
			}
		}
		double tp = 0.0;
		double fn = 0.0;
		double fp = 0.0;
		double precision = 0.0;
		double recall = 0.0;

		if(retrieved.size()==0&&expected.size()==0){
			tp = 1;
		}

		for (int i = 0; i < retrieved.size(); i++) {
			if (expected.contains(retrieved.get(i))) {
				tp++;
			} else {
				fp++;
			}
		}

		for (int i = 0; i < expected.size(); i++) {
			if (!retrieved.contains(expected.get(i))) {
				fn++;
			}
		}

		if ((tp + fp) > 0) {
			precision = tp / (tp + fp);
		}
		if ((tp + fn) > 0) {
			recall = tp / (tp + fn);
		}

		res = precision + "/" + recall;
		return res;
	}

	/**
	 * 
	 * Load all the services in a given path It is used to calculate search
	 * accuracy with the registered services in the experiments
	 */
	private static ArrayList<ServiceDescription> loadServices(String path) {
		ArrayList<ServiceDescription> services = new ArrayList<ServiceDescription>();
		File servicesFolder = new File(path);
		File[] servicesFiles = servicesFolder.listFiles();
		JSONParser parser = new JSONParser();
		for (int i = 0; i < servicesFiles.length; i++) {
			try {
				FileReader reader = new FileReader(servicesFiles[i].getAbsolutePath());
				Object obj = parser.parse(reader);
				JSONObject jsonObject = (JSONObject) obj;
				reader.close();
				ServiceDescription service = Util.parseJsonToServiceDescription(jsonObject);
				services.add(service);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return services;
	}

	/**
	 * 
	 * Gets service names from a JSON ojbect
	 */
	private static String getServicesNames(JSONObject object) {
		String res = "";
		if (object.containsKey("plan")) {
			JSONObject plan = (JSONObject) object.get("plan");
			res = res + getServicesNames(plan);
		}
		if (object.containsKey("steps")) {
			JSONArray steps = (JSONArray) object.get("steps");
			for (int i = steps.size() - 1; i >= 0; i--) {
				JSONObject step = (JSONObject) steps.get(i);
				if(step.get("type").equals("SimpleStep")){
					res = res + getServicesNames((JSONObject) steps.get(i));
				}
				if(step.get("type").equals("SequentialStep")){
					res = res + "[" + getServicesNames((JSONObject) steps.get(i)) + "]";
				}
				if(step.get("type").equals("ParallelStep")){
					res = res + "{" +getServicesNames((JSONObject) steps.get(i)) + "}";
				}
			}
		}
		if (object.containsKey("serviceDescription")) {
			JSONObject desc = (JSONObject) object.get("serviceDescription");
			res = res + "<" + (String) desc.get("name") + ">";
		}
		return res;
	}
	
	/**
	 * 
	 * Put all results together
	 */
	@SuppressWarnings("deprecation")
	public static void mergeResults(String folder, String nameSummary, String approach) {
		try{
			File folderResults = new File(folder);
			File[] resultsFiles = folderResults.listFiles();
			boolean first = true;
			File generalFile = new File("/home/ubuntu/Repositories/SmartCitySD/Results/service-models-results/"+nameSummary);
			generalFile.getParentFile().mkdirs();
			generalFile.createNewFile();
			int totalRows = 0;
			int r = 0;
			XSSFWorkbook generalSummary = new XSSFWorkbook();
			for (int i = 0; i < resultsFiles.length; i++) {
				File resultsFile = resultsFiles[i];
				System.out.println(approach+"::"+resultsFile.getName());
				XSSFWorkbook summary = new XSSFWorkbook(resultsFile);
				Iterator<Sheet> sheets = summary.iterator();
				while(sheets.hasNext()){
					r = 0;
					Sheet sheet = sheets.next();
					Sheet generalSheet = null;
					if(first){
						generalSheet = generalSummary.createSheet(sheet.getSheetName());
					}else{
						generalSheet = generalSummary.getSheet(sheet.getSheetName());
					}
					Iterator<Row> rows = sheet.rowIterator();
					while(rows.hasNext()){
						Row row = rows.next();
						if(!first && row.getRowNum()==0)
							row = rows.next();
						Row generalRow = generalSheet.createRow(r+totalRows);
						Iterator<Cell> cells = row.cellIterator();
						while(cells.hasNext()){
							Cell cell = cells.next();
							Cell generalCell = generalRow.createCell(cell.getColumnIndex());
							if(cell.getCellType()==Cell.CELL_TYPE_STRING)
								generalCell.setCellValue(cell.getStringCellValue());
							else if(cell.getCellType()==Cell.CELL_TYPE_NUMERIC)
								generalCell.setCellValue(cell.getNumericCellValue());
							else
								generalCell.setCellValue(0);
						}
						r ++;
					}
				}
				totalRows = totalRows + r;
				first = false;
				summary.close();
			}
			FileOutputStream fileOut = new FileOutputStream(generalFile);
			generalSummary.write(fileOut);
			fileOut.close();
			generalSummary.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	public static void jointPaperResults(String file) {
		try{
			File resultsFile = new File(file);
			XSSFWorkbook results = new XSSFWorkbook(resultsFile);
			Sheet sheet1 = results.getSheet("Sheet1");
			Sheet sheet2 = results.getSheet("Sheet2");
		
			Iterator<Row> rowsSheet2 = sheet2.rowIterator();
			boolean firsts2 = true;
			while(rowsSheet2.hasNext()){
				Row rowSheet2 = rowsSheet2.next();
				if(firsts2){
					rowSheet2 = rowsSheet2.next();
					firsts2 = false;
				}
				String mobilitys2 = rowSheet2.getCell(0).getStringCellValue();
				int gatewayss2 = (int) rowSheet2.getCell(1).getNumericCellValue();
				int alternativess2 = (int) rowSheet2.getCell(2).getNumericCellValue();
				int servicess2 = (int) rowSheet2.getCell(3).getNumericCellValue();
				int lengths2 = (int) rowSheet2.getCell(4).getNumericCellValue();
				System.out.println("::mobility:"+mobilitys2+"::gateways:"+gatewayss2+"::alternatives:"+alternativess2+"::services:"+servicess2+"::length:"+lengths2+"::");
				Iterator<Row> rowsSheet1 = sheet1.rowIterator();
				boolean firsts1 = true;
				ArrayList<Double> values = new ArrayList<Double>();
				while(rowsSheet1.hasNext()){
					Row rowSheet1 = rowsSheet1.next();
					if(firsts1){
						rowSheet1 = rowsSheet1.next();
						firsts1 = false;
					}
					String mobilitys1 = rowSheet1.getCell(0).getStringCellValue();
					int gatewayss1 = (int) rowSheet1.getCell(1).getNumericCellValue();
					int alternativess1 = (int) rowSheet1.getCell(2).getNumericCellValue();
					int servicess1 = (int) rowSheet1.getCell(3).getNumericCellValue();
					int lengths1 = (int) rowSheet1.getCell(4).getNumericCellValue();
					
					if(mobilitys2.equals(mobilitys1)&&gatewayss2==gatewayss1&&alternativess2==alternativess1&&servicess2==servicess1&&lengths2==lengths1){
						values.add(rowSheet1.getCell(5).getNumericCellValue());
					}
				}
				
				double [] array = new double[values.size()];
				for(int i=0; i<values.size(); i++){
					array[i]=values.get(i);
				}
				
				Mean mean = new Mean();
				mean.setData(array);
				double arrayMean = mean.evaluate();
				Cell dataCell=rowSheet2.createCell(5);
				dataCell.setCellValue(arrayMean);
				
				StandardDeviation sd = new StandardDeviation();
				sd.setData(array);
				double arraySd = sd.evaluate();
				dataCell=rowSheet2.createCell(6);
				dataCell.setCellValue(arraySd);
				
				Min min = new Min();
				min.setData(array);
				double arrayMin = min.evaluate();
				dataCell=rowSheet2.createCell(7);
				dataCell.setCellValue(arrayMin);
				
				Percentile percentile = new Percentile();
				percentile.setData(array);
				double arrayq1 = percentile.evaluate(25);
				dataCell=rowSheet2.createCell(8);
				dataCell.setCellValue(arrayq1);
				
				Median median = new Median();
				median.setData(array);
				double arrayMedian = median.evaluate();
				dataCell=rowSheet2.createCell(9);
				dataCell.setCellValue(arrayMedian);
				
				percentile = new Percentile();
				percentile.setData(array);
				double arrayq3 = percentile.evaluate(75);
				dataCell=rowSheet2.createCell(10);
				dataCell.setCellValue(arrayq3);
				
				Max max = new Max();
				max.setData(array);
				double arrayMax = max.evaluate();
				dataCell=rowSheet2.createCell(11);
				dataCell.setCellValue(arrayMax);
			}
			File summary = new File(file.replace(".xlsx", "_summary.xlsx"));
			summary.getParentFile().mkdirs();
			summary.createNewFile();
			FileOutputStream fileOut = new FileOutputStream(summary);
			results.write(fileOut);
			fileOut.close();
			results.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public static void createSummaryUtilities(String folder, String approach) {
		try{
			File folderResults = new File(folder);
			File[] resultsFiles = folderResults.listFiles();
			for(int j=0;j<resultsFiles.length;j++){
				File resultsFile = resultsFiles[j];
				System.out.println("Processing file: " + resultsFile.getName());
				XSSFWorkbook utilityFile = new XSSFWorkbook(resultsFile);
				XSSFWorkbook summaryFile = new XSSFWorkbook();
				int sheets = utilityFile.getNumberOfSheets();
				Sheet utility = summaryFile.createSheet("utility");
				for(int i=1; i<=sheets; i++){
					//Row headerRow = utility.createRow(0);
					Sheet det = utilityFile.getSheet("detail-"+i);
					if(i==1){
						int rows = det.getLastRowNum();
						for(int x=0;x<=rows;x++){
							Row r = det.getRow(x);
							Cell c = r.getCell(0);
							if(x==0){
								String value = c.getStringCellValue();
								Row ur = utility.createRow(x);
								Cell uc = ur.createCell(0);
								uc.setCellValue(value);
							}else{
								double value = c.getNumericCellValue();
								Row ur = utility.createRow(x);
								Cell uc = ur.createCell(0);
								uc.setCellValue(value);
							}
						}
					}
					
					/*Cell headerCell=headerRow.createCell(i);
					headerCell.setCellValue("utility-"+i);
					
					int rows = det.getLastRowNum();
					for(int x=0;x<=rows;x++){
						Row r = det.getRow(x);
						Cell c = r.getCell(1);
						if(x==0){
							String value = c.getStringCellValue();
							Row ur = utility.getRow(x);
							Cell uc = ur.createCell(i);
							uc.setCellValue(value);
						}else{
							double value = c.getNumericCellValue();
							Row ur = utility.getRow(x);
							Cell uc = ur.createCell(i);
							uc.setCellValue(value);
						}
					}*/
				}
				
				
				/*Row hr = utility.getRow(0);
				Cell hc = hr.createCell(sheets+1);
				hc.setCellValue("mean");
				hc = hr.createCell(sheets+2);
				hc.setCellValue("deviation");
				for(int i=1; i<utility.getLastRowNum();i++){
					Row r = utility.getRow(i);
					double [] values = new double [r.getLastCellNum()];  
					for(int x=0;x<values.length;x++){
						Cell c = r.getCell(x);
						values[x] = c.getNumericCellValue();
					}
					int col = sheets + 1;
					Mean mean = new Mean();
					mean.setData(values);
					double vMean = mean.evaluate();
					hc = r.createCell(col);
					hc.setCellValue(vMean);
					col++;
					hc = r.createCell(col);
					hc.setCellValue("standard");
					StandardDeviation sd = new StandardDeviation();
					sd.setData(values);
					double vSd = sd.evaluate();
					hc = r.createCell(col);
					hc.setCellValue(vSd);
				}*/
				
				String nameFile = resultsFile.getName().replace(".xlsx", "_summary.xlsx");
				nameFile = "/home/ubuntu/Repositories/SmartCitySD/Results/service-models-results/utility-summary/"+ approach +"/" + nameFile; 
				File summary = new File(nameFile);
				summary.getParentFile().mkdirs();
				summary.createNewFile();
				FileOutputStream fileOut = new FileOutputStream(summary);
				summaryFile.write(fileOut);
				fileOut.close();
				summaryFile.close();
				utilityFile.close();
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

}
