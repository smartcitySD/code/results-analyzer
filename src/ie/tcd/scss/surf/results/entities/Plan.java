/**
 * Author: Christian Cabrera
 * This class represents a service plan produced in the discovery process
 */

package ie.tcd.scss.surf.results.entities;

import java.util.ArrayList;

public class Plan {

	private ArrayList<ServiceParameter> inputs;
	private ArrayList<ServiceParameter> outputs;
	private int state;
	private ArrayList<Step> steps;
	private ArrayList<Domain> domains;

	public Plan() {
		inputs = new ArrayList<ServiceParameter>();
		outputs = new ArrayList<ServiceParameter>();
		state = 0;
		steps = new ArrayList<Step>();
		domains = new ArrayList<Domain>();
	}

	public Plan(ArrayList<ServiceParameter> inputs, ArrayList<ServiceParameter> outputs, int state,
			ArrayList<Step> steps) {
		this.setInputs(inputs);
		this.setOutputs(outputs);
		this.setState(state);
		this.setSteps(steps);
	}

	public ArrayList<ServiceParameter> getInputs() {
		return inputs;
	}

	public void setInputs(ArrayList<ServiceParameter> inputs) {
		this.inputs = inputs;
	}

	public ArrayList<ServiceParameter> getOutputs() {
		return outputs;
	}

	public void setOutputs(ArrayList<ServiceParameter> outputs) {
		this.outputs = outputs;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public ArrayList<Step> getSteps() {
		return steps;
	}

	public void setSteps(ArrayList<Step> steps) {
		this.steps = steps;
	}

	public ArrayList<Domain> getDomains() {
		return domains;
	}

	public void setDomains(ArrayList<Domain> domains) {
		this.domains = domains;
	}

}
