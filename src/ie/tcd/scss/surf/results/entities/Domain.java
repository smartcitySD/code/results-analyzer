/**
 * Author: Christian Cabrera
 * This entity represents a domain in the service description
 */

package ie.tcd.scss.surf.results.entities;

public class Domain {
	private String name;
	private String url;


	public Domain() {
		this.setName("");
		this.setUrl("");
	}

	public Domain(String name, String url) {
		this.setName(name);
		this.setUrl(url);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
