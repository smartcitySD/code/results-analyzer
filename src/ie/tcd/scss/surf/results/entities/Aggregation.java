/**
 * Author: Christian Cabrera
 * Class that represents the aggregation function of a service
 */

package ie.tcd.scss.surf.results.entities;

public class Aggregation {
	private String function;
	private String perNode;
	private String window;

	public Aggregation() {
		this.setFunction("");
		this.setPerNode("");
		this.setWindow("");
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getPerNode() {
		return perNode;
	}

	public void setPerNode(String perNode) {
		this.perNode = perNode;
	}

	public String getWindow() {
		return window;
	}

	public void setWindow(String window) {
		this.window = window;
	}

}
