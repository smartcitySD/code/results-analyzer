/**
 * Author: Christian Cabrera
 * This class represents a simple step in a plan
 * It is basically one service
 */

package ie.tcd.scss.surf.results.entities;

import java.util.ArrayList;

public class SimpleStep extends Step {
	private ServiceDescription service;

	public SimpleStep() {
		super();
		service = new ServiceDescription();
	}

	public SimpleStep(ArrayList<ServiceParameter> inputs, ArrayList<ServiceParameter> outputs, int order, ServiceDescription service) {
		super(inputs, outputs, order, "SimpleStep");
		this.setService(service);
	}

	public ServiceDescription getService() {
		return service;
	}

	public void setService(ServiceDescription service) {
		this.service = service;
	}

}
