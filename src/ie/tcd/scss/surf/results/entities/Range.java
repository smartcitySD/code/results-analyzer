/**
 * Author: Christian Cabrera
 * Class that represents a range for a negotiation constraint
 */

package ie.tcd.scss.surf.results.entities;

public class Range extends NegotiationConstraint {
	private String min;
	private String max;

	public Range() {
		super();
		this.setMin("");
		this.setMax("");
	}

	public Range(String type, String min, String max) {
		super(type);
		this.setMin(min);
		this.setMax(max);
	}

	public String getMin() {
		return min;
	}

	public void setMin(String min) {
		this.min = min;
	}

	public String getMax() {
		return max;
	}

	public void setMax(String max) {
		this.max = max;
	}

}
