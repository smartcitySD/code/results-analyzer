/**
 * Author: Christian Cabrera
 * Abstract class that represents a step in a service plan
 */

package ie.tcd.scss.surf.results.entities;

import java.util.ArrayList;

public abstract class Step {

	private ArrayList<ServiceParameter> inputs;
	private ArrayList<ServiceParameter> outputs;
	private int order;
	private String type;

	public Step() {
		inputs = new ArrayList<ServiceParameter>();
		outputs = new ArrayList<ServiceParameter>();
		order = 0;
	}

	public Step(ArrayList<ServiceParameter> inputs, ArrayList<ServiceParameter> outputs, int order, String type) {
		this.setInputs(inputs);
		this.setOutputs(outputs);
		this.setOrder(order);
		this.setType(type);
	}

	public ArrayList<ServiceParameter> getInputs() {
		return inputs;
	}

	public void setInputs(ArrayList<ServiceParameter> inputs) {
		this.inputs = inputs;
	}

	public ArrayList<ServiceParameter> getOutputs() {
		return outputs;
	}

	public void setOutputs(ArrayList<ServiceParameter> outputs) {
		this.outputs = outputs;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
