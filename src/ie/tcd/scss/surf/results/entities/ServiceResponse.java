/**
 * Author: Christian Cabrera
 * This entity represents a service response in the service model
 */


package ie.tcd.scss.surf.results.entities;

import java.util.ArrayList;

public class ServiceResponse{

	private String jobId;
	private ArrayList<ServiceParameter> inputs;
	private ArrayList<ServiceParameter> outputs;
	private ArrayList<Domain> domains;
	private ArrayList<QoSParameter> qosParameters;

	public ServiceResponse() {
		this.setJobId("");
		this.setInputs(new ArrayList<ServiceParameter>());
		this.setOutputs(new ArrayList<ServiceParameter>());
		this.setDomains(new ArrayList<Domain>());
		this.setQosParameters(new ArrayList<QoSParameter>());
	}

	public ServiceResponse(String jobId, ArrayList<ServiceParameter> inputs, ArrayList<ServiceParameter> outputs,
			ArrayList<Domain> domains, ArrayList<QoSParameter> qosParameters) {
		this.setJobId(jobId);
		this.setInputs(inputs);
		this.setOutputs(outputs);
		this.setDomains(domains);
		this.setQosParameters(qosParameters);
	}

	public ArrayList<ServiceParameter> getInputs() {
		return inputs;
	}

	public void setInputs(ArrayList<ServiceParameter> inputs) {
		this.inputs = inputs;
	}

	public ArrayList<ServiceParameter> getOutputs() {
		return outputs;
	}

	public void setOutputs(ArrayList<ServiceParameter> outputs) {
		this.outputs = outputs;
	}

	public ArrayList<QoSParameter> getQosParameters() {
		return qosParameters;
	}

	public void setQosParameters(ArrayList<QoSParameter> qosParameters) {
		this.qosParameters = qosParameters;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getJobId() {
		return jobId;
	}

	public ArrayList<Domain> getDomains() {
		return domains;
	}

	public void setDomains(ArrayList<Domain> domains) {
		this.domains = domains;
	}

}
