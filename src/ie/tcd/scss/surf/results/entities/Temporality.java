/**
 * Author: Christian Cabrera
 * Class that represents the temporality of a service
 */

package ie.tcd.scss.surf.results.entities;

public class Temporality {
	private String start;
	private long duration;
	private String unit;
	private String negotiable;
	private NegotiationConstraint negotiationConstraints;

	public Temporality() {
		this.setStart("");
		this.setDuration(0);
		this.setUnit("");
		this.setNegotiable("");
	}

	public Temporality(String start, long duration, String unit, String netotiale,
			NegotiationConstraint negotiationConstraints) {
		this.setStart(start);
		this.setDuration(duration);
		this.setUnit(unit);
		this.setNegotiable(negotiable);
		this.setNegotiationConstraints(negotiationConstraints);
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public String getNegotiable() {
		return negotiable;
	}

	public void setNegotiable(String negotiable) {
		this.negotiable = negotiable;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public NegotiationConstraint getNegotiationConstraints() {
		return negotiationConstraints;
	}

	public void setNegotiationConstraints(NegotiationConstraint negotiationConstraints) {
		this.negotiationConstraints = negotiationConstraints;
	}

}
