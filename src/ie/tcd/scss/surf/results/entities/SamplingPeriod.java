/**
 * Author: Christian Cabrera
 * Class that represents the sampling period of a service
 */

package ie.tcd.scss.surf.results.entities;

public class SamplingPeriod {
	private long value;
	private String description;
	private String unit;
	private String negotiable;
	private NegotiationConstraint negotiationConstraints;

	public SamplingPeriod() {
		this.setValue(0);
		this.setDescription("");
		this.setUnit("");
		this.setNegotiable("");
	}

	public SamplingPeriod(long value, String description, String unit, String negotiable,
			NegotiationConstraint negotiationConstraints) {
		this.setValue(value);
		this.setDescription(description);
		this.setUnit(unit);
		this.setNegotiable(negotiable);
		this.setNegotiationConstraints(negotiationConstraints);
	}

	public long getValue() {
		return value;
	}

	public void setValue(long value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getNegotiable() {
		return negotiable;
	}

	public void setNegotiable(String negotiable) {
		this.negotiable = negotiable;
	}

	public NegotiationConstraint getNegotiationConstraints() {
		return negotiationConstraints;
	}

	public void setNegotiationConstraints(NegotiationConstraint negotiationConstraints) {
		this.negotiationConstraints = negotiationConstraints;
	}
}
