/**
 * Author: Christian Cabrera
 * Class that represents the type of a service
 */

package ie.tcd.scss.surf.results.entities;

public class Type {
	private String physicalProcess;
	private String direction;
	private String dataType;
	private String unit;

	public Type() {
		this.setPhysicalProcess("");
		this.setDirection("");
		this.setDataType("");
		this.setUnit("");
	}

	public Type(String physicalProcess, String direction, String dataType, String unit) {
		this.setPhysicalProcess(physicalProcess);
		this.setDirection(direction);
		this.setDataType(dataType);
		this.setUnit(unit);
	}

	public String getPhysicalProcess() {
		return physicalProcess;
	}

	public void setPhysicalProcess(String physicalProcess) {
		this.physicalProcess = physicalProcess;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

}
