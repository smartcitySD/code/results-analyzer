/**
 * Author: Christian Cabrera
 * Class that represents the location of a service
 */

package ie.tcd.scss.surf.results.entities;

public class Location {
	private String description;
	private String region;
	private String negotiable;
	private NegotiationConstraint negotiationConstraints;
	private Coordinate coordinate;

	public Location() {
		this.setDescription("");
		this.setRegion("");
		this.setNegotiable("");
		this.setCoordinate(new Coordinate());
	}

	public Location(String description, String region, String negotiable,
 NegotiationConstraint negotiationConstraints,
			Coordinate coordinate) {
		this.setDescription(description);
		this.setRegion(region);
		this.setNegotiable(negotiable);
		this.setNegotiationConstraints(negotiationConstraints);
		this.setCoordinate(coordinate);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getNegotiable() {
		return negotiable;
	}

	public void setNegotiable(String negiotable) {
		this.negotiable = negiotable;
	}

	public NegotiationConstraint getNegotiationConstraints() {
		return negotiationConstraints;
	}

	public void setNegotiationConstraints(NegotiationConstraint negotiationConstraints) {
		this.negotiationConstraints = negotiationConstraints;
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}

}
