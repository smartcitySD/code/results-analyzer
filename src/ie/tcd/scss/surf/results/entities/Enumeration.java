/**
 * Author: Christian Cabrera
 * Class that represents an enumeration for a negotation constraint
 */

package ie.tcd.scss.surf.results.entities;

public class Enumeration extends NegotiationConstraint {
	private String[] values;

	public Enumeration() {
		super();
		this.setValues(new String[0]);
	}

	public Enumeration(String type, String[] values) {
		super(type);
		this.setValues(values);
	}

	public String[] getValues() {
		return values;
	}

	public void setValues(String[] values) {
		this.values = values;
	}

}
