package ie.tcd.scss.surf.results.main;

import ie.tcd.scss.surf.results.analyzer.SDExperimentsAnalyzer;

public class Main {

	public static void main(String[] args) {
		//SDExperimentsAnalyzer.jointPaperResults("/home/ubuntu/Repositories/SmartCitySD/Results/paper-cit-tcd/results/qos_optimisation/summary_sd_qos_mobile2.xlsx");
		//SDExperimentsAnalyzer.mergeResults("/home/ubuntu/Repositories/SmartCitySD/Results/service-models-results/location-summary/", "location-summary.xlsx","location");
		//SDExperimentsAnalyzer.mergeResults("/home/ubuntu/Repositories/SmartCitySD/Results/service-models-results/domain-summary/", "domain-summary.xlsx","domain");
		//SDExperimentsAnalyzer.mergeResults("/home/ubuntu/Repositories/SmartCitySD/Results/service-models-results/urban-summary/", "urban-summary.xlsx","urban");
		SDExperimentsAnalyzer.createSummaryUtilities("/home/ubuntu/Repositories/SmartCitySD/Results/service-models-results/utility-urban/","urban");
		//SDExperimentsAnalyzer.createSummaryUtilities("/home/ubuntu/Repositories/SmartCitySD/Results/service-models-results/utility-location/","location");
		//SDExperimentsAnalyzer.createSummaryUtilities("/home/ubuntu/Repositories/SmartCitySD/Results/service-models-results/utility-domain/","domain");
		System.exit(0);
	}
}
