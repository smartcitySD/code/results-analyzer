package ie.tcd.scss.surf.results.matchmaker;

import java.util.ArrayList;

import ie.tcd.scss.surf.results.entities.Domain;
import ie.tcd.scss.surf.results.entities.ComposedStep;
import ie.tcd.scss.surf.results.entities.ParallelStep;
import ie.tcd.scss.surf.results.entities.Plan;
import ie.tcd.scss.surf.results.entities.RequestDescription;
import ie.tcd.scss.surf.results.entities.SequentialStep;
import ie.tcd.scss.surf.results.entities.ServiceDescription;
import ie.tcd.scss.surf.results.entities.ServiceParameter;
import ie.tcd.scss.surf.results.entities.SimpleStep;
import ie.tcd.scss.surf.results.entities.Step;

public class Matchmaker {
	public Matchmaker() {

	}

	public ArrayList<Plan> backwardPlanning(ArrayList<ServiceDescription> services,
			RequestDescription request, ArrayList<Plan> currentPlans) {
		ArrayList<Plan> plans = new ArrayList<Plan>();
		if(currentPlans.size()==0){
			ArrayList<ServiceParameter> requestInputs = request.getInputs();
			ArrayList<ServiceParameter> requestOutputs = request.getOutputs();
			for (int i = 0; i < services.size(); i++) {
				ArrayList<ServiceParameter> serviceInputs = services.get(i).getInputs();
				ArrayList<ServiceParameter> serviceOutputs = services.get(i).getOutputs();
				ArrayList<ServiceParameter> comparedOutputs = compareParameters(requestOutputs, serviceOutputs);
				if (comparedOutputs.size() == 0) {
					Plan plan = new Plan();
					plan.setInputs(serviceInputs);
					plan.setOutputs(requestOutputs);
					ArrayList<ServiceParameter> comparedInputs = compareParameters(requestInputs, serviceInputs);
					if (comparedInputs.size() == 0 || comparedInputs.size() < requestInputs.size()) {
						SimpleStep step = new SimpleStep();
						step.setType("SimpleStep");
						step.setInputs(requestInputs);
						step.setOutputs(requestOutputs);
						step.setService(services.get(i));
						step.setOrder(0);
						plan.getSteps().add(step);
						plan.setState(1);
						for(int j=0; j<services.get(i).getDomains().size(); j++){
							if(!domainIncluded(plan.getDomains(),services.get(i).getDomains().get(j)))
								plan.getDomains().add(services.get(i).getDomains().get(j));
						}
						plans.add(plan);
					}
					if (comparedInputs.size() == requestInputs.size()) {
						SequentialStep sequence = new SequentialStep();
						sequence.setType("SequentialStep");
						sequence.setOrder(0);
						sequence.setInputs(serviceInputs);
						sequence.setOutputs(serviceOutputs);
						SimpleStep step = new SimpleStep();
						step.setType("SimpleStep");
						step.setOrder(0);
						step.setInputs(serviceInputs);
						step.setOutputs(serviceOutputs);
						step.setService(services.get(i));
						sequence.getSteps().add(step);
						plan.getSteps().add(sequence);
						plan.setState(0);
						for(int j=0; j<services.get(i).getDomains().size(); j++){
							if(!domainIncluded(plan.getDomains(),services.get(i).getDomains().get(j)))
								plan.getDomains().add(services.get(i).getDomains().get(j));
						}
						ArrayList<Plan> tempPlans = new ArrayList<Plan>();
						tempPlans.add(plan);
						tempPlans = backwardPlanning(services, request, tempPlans);
						for (int j = 0; j < tempPlans.size(); j++) {
							plans.add(tempPlans.get(j));
						}
					}
				}
			}
		}
		else{
			for (int x = 0; x < currentPlans.size(); x++) {
				Plan p = new Plan();
				p.setState(currentPlans.get(x).getState());
				for (int k = 0; k < currentPlans.get(x).getInputs().size(); k++) {
					p.getInputs().add(currentPlans.get(x).getInputs().get(k));
				}
				for (int k = 0; k < currentPlans.get(x).getOutputs().size(); k++) {
					p.getOutputs().add(currentPlans.get(x).getOutputs().get(k));
				}
				for (int k = 0; k < currentPlans.get(x).getSteps().size(); k++) {
					p.getSteps().add(getSteps(currentPlans.get(x).getSteps().get(k)));
				}
				for (int k = 0; k < currentPlans.get(x).getDomains().size(); k++) {
					p.getDomains().add(currentPlans.get(x).getDomains().get(k));
				}
				plans.add(p);
			}
			for (int x = 0; x < currentPlans.size(); x++) {
				ArrayList<ServiceParameter> requestInputs = request.getInputs();
				ArrayList<ServiceParameter> requestOutputs = new ArrayList<ServiceParameter>();
				
				if (currentPlans.get(x).getSteps().get(currentPlans.get(x).getSteps().size() - 1)
						.getClass() == SequentialStep.class) {
					SequentialStep s = (SequentialStep) currentPlans.get(x).getSteps()
							.get(currentPlans.get(x).getSteps().size() - 1);
					SequentialStep sequence = new SequentialStep();
					sequence.setType("SequentialStep");
					sequence.setOrder(s.getOrder());
					for (int k = 0; k < s.getInputs().size(); k++) {
						sequence.getInputs().add(s.getInputs().get(k));
						requestOutputs.add(s.getInputs().get(k));
					}

					for (int k = 0; k < s.getOutputs().size(); k++) {
						sequence.getOutputs().add(s.getOutputs().get(k));
					}
					for (int k = 0; k < s.getSteps().size(); k++) {
						sequence.getSteps().add((SimpleStep) getSteps(s.getSteps().get(k)));
					}
				}

				if (currentPlans.get(x).getSteps().get(currentPlans.get(x).getSteps().size() - 1)
						.getClass() == ParallelStep.class) {
					ParallelStep p = (ParallelStep) currentPlans.get(x).getSteps()
							.get(currentPlans.get(x).getSteps().size() - 1);
					ParallelStep parallel = new ParallelStep();
					parallel.setType("ParallelStep");
					parallel.setOrder(p.getOrder());
					
					for (int k = 0; k < p.getOutputs().size(); k++) {
						parallel.getOutputs().add(p.getOutputs().get(k));
					}
					for (int k = 0; k < p.getSteps().size(); k++) {
						parallel.getSteps().add((ComposedStep) getSteps(p.getSteps().get(k)));
					}

					if (p.getRemainingOutputs().size() > 0) {
						for (int k = 0; k < p.getRemainingOutputs().size(); k++) {
							parallel.getRemainingOutputs().add(p.getRemainingOutputs().get(k));
							requestOutputs.add(p.getRemainingOutputs().get(k));
						}
					} else {
						for (int k = 0; k < p.getInputs().size(); k++) {
							parallel.getInputs().add(p.getInputs().get(k));
							requestOutputs.add(p.getInputs().get(k));
						}

					}
				}

				for (int i = 0; i < services.size(); i++) {
					ArrayList<ServiceParameter> serviceInputs = services.get(i).getInputs();
					ArrayList<ServiceParameter> serviceOutputs = services.get(i).getOutputs();
					ArrayList<ServiceParameter> comparedOutputs = compareParameters(requestOutputs, serviceOutputs);
					if (comparedOutputs.size() == 0) {
						Plan plan = new Plan();
						plan.setState(currentPlans.get(x).getState());
						for (int k = 0; k < currentPlans.get(x).getInputs().size(); k++) {
							plan.getInputs().add(currentPlans.get(x).getInputs().get(k));
						}
						for (int k = 0; k < currentPlans.get(x).getOutputs().size(); k++) {
							plan.getOutputs().add(currentPlans.get(x).getOutputs().get(k));
						}
						for (int k = 0; k < currentPlans.get(x).getSteps().size(); k++) {
							plan.getSteps().add(getSteps(currentPlans.get(x).getSteps().get(k)));
						}
						for (int k = 0; k < currentPlans.get(x).getDomains().size(); k++) {
							plan.getDomains().add(currentPlans.get(x).getDomains().get(k));
						}

						if (plan.getSteps().get(plan.getSteps().size() - 1).getClass() == SequentialStep.class) {

							SequentialStep s = (SequentialStep) plan.getSteps().get(plan.getSteps().size() - 1);
							SequentialStep sequence = new SequentialStep();
							sequence.setType("SequentialStep");
							sequence.setOrder(s.getOrder());
							for (int k = 0; k < s.getInputs().size(); k++) {
								sequence.getInputs().add(s.getInputs().get(k));
								requestOutputs.add(s.getInputs().get(k));
							}

							for (int k = 0; k < s.getOutputs().size(); k++) {
								sequence.getOutputs().add(s.getOutputs().get(k));
							}
							for (int k = 0; k < s.getSteps().size(); k++) {
								sequence.getSteps().add((SimpleStep) getSteps(s.getSteps().get(k)));
							}

							SimpleStep step = new SimpleStep();
							step.setType("SimpleStep");
							step.setInputs(serviceInputs);
							step.setOutputs(serviceOutputs);
							step.setService(services.get(i));
							step.setOrder(sequence.getSteps().size());
							sequence.getSteps().add(step);
							sequence.setInputs(serviceInputs);
							ArrayList<ServiceParameter> comparedInputs = compareParameters(requestInputs,
									serviceInputs);

							if (comparedInputs.size() == 0 || comparedInputs.size() < requestInputs.size()) {
								plan.getSteps().remove(plan.getSteps().size() - 1);
								plan.getSteps().add(sequence);
								plan.setInputs(serviceInputs);
								plan.setState(1);
								for(int j=0; j<services.get(i).getDomains().size(); j++){
									if(!domainIncluded(plan.getDomains(),services.get(i).getDomains().get(j)))
										plan.getDomains().add(services.get(i).getDomains().get(j));
								}
								plans.add(plan);
							}
							if (comparedInputs.size() == requestInputs.size()) {
								plan.getSteps().remove(plan.getSteps().size() - 1);
								plan.getSteps().add(sequence);
								plan.setInputs(serviceInputs);
								plan.setState(0);
								for(int j=0; j<services.get(i).getDomains().size(); j++){
									if(!domainIncluded(plan.getDomains(),services.get(i).getDomains().get(j)))
										plan.getDomains().add(services.get(i).getDomains().get(j));
								}
								ArrayList<Plan> tempPlans = new ArrayList<Plan>();
								tempPlans.add(plan);
								tempPlans = backwardPlanning(services, request, tempPlans);
								for (int j = 0; j < tempPlans.size(); j++) {
									plans.add(tempPlans.get(j));
								}
							}

						}

						if (plan.getSteps().get(plan.getSteps().size() - 1).getClass() == ParallelStep.class) {

							ParallelStep p = (ParallelStep) plan.getSteps().get(plan.getSteps().size() - 1);
							ParallelStep parallel = new ParallelStep();
							parallel.setType("ParallelStep");
							parallel.setOrder(p.getOrder());
							
							for (int k = 0; k < p.getOutputs().size(); k++) {
								parallel.getOutputs().add(p.getOutputs().get(k));
							}
							for (int k = 0; k < p.getInputs().size(); k++) {
								parallel.getInputs().add(p.getInputs().get(k));
							}
							for (int k = 0; k < p.getRemainingOutputs().size(); k++) {
								parallel.getRemainingOutputs().add(p.getRemainingOutputs().get(k));
							}
							for (int k = 0; k < p.getSolvedOutputs().size(); k++) {
								parallel.getSolvedOutputs().add(p.getSolvedOutputs().get(k));
							}
							for (int k = 0; k < p.getSteps().size(); k++) {
								parallel.getSteps().add((ComposedStep) getSteps(p.getSteps().get(k)));
							}

							SequentialStep sequence = new SequentialStep();
							sequence.setType("SequentialStep");
							sequence.setInputs(serviceInputs);
							sequence.setOutputs(serviceOutputs);
							SimpleStep step = new SimpleStep();
							step.setType("SimpleStep");
							step.setOrder(sequence.getSteps().size());
							step.setInputs(serviceInputs);
							step.setOutputs(serviceOutputs);
							step.setService(services.get(i));
							sequence.getSteps().add(step);
							if (parallel.getRemainingOutputs().size() > 0) {
								sequence.setOrder(parallel.getSteps().get(0).getOrder());
								parallel.getSteps().add(sequence);
								parallel.getSolvedOutputs().addAll(serviceOutputs);
								for (int j = 0; j < parallel.getRemainingOutputs().size(); j++) {
									int b = 0;
									for (int k = 0; k < serviceOutputs.size(); k++) {
										if (parallel.getRemainingOutputs().get(j).getName()
												.equals(serviceOutputs.get(k).getName())
												&& parallel.getRemainingOutputs().get(j).getName()
														.equals(serviceOutputs.get(k).getName())) {
											b = 1;
											break;
										}
									}
									if (b == 1) {
										parallel.getRemainingOutputs().remove(j);
									}
								}
								parallel.getInputs().addAll(serviceInputs);

								ArrayList<ServiceParameter> comparedInputs = compareParameters(requestInputs,
										parallel.getInputs());
								if (comparedInputs.size() == 0 || comparedInputs.size() < requestInputs.size()) {
									plan.getSteps().remove(plan.getSteps().size() - 1);
									plan.getSteps().add(parallel);
									if (parallel.getRemainingOutputs().size() > 0) {
										plan.setInputs(parallel.getRemainingOutputs());
									} else {
										plan.setInputs(new ArrayList<ServiceParameter>());
										for (int j = 0; j < parallel.getSteps().size(); j++) {
											for (int k = 0; k < parallel.getSteps().get(j).getInputs().size(); k++) {
												plan.getInputs().add(parallel.getSteps().get(j).getInputs().get(k));
											}
										}
									}
									for(int j=0; j<services.get(i).getDomains().size(); j++){
										if(!domainIncluded(plan.getDomains(),services.get(i).getDomains().get(j)))
											plan.getDomains().add(services.get(i).getDomains().get(j));
									}
									plan.setState(1);
									plans.add(plan);
								}
								if (comparedInputs.size() == requestInputs.size()) {
									plan.getSteps().remove(plan.getSteps().size() - 1);
									plan.getSteps().add(parallel);
									plan.setInputs(serviceInputs);
									plan.setState(0);
									for(int j=0; j<services.get(i).getDomains().size(); j++){
										if(!domainIncluded(plan.getDomains(),services.get(i).getDomains().get(j)))
											plan.getDomains().add(services.get(i).getDomains().get(j));
									}
									ArrayList<Plan> tempPlans = new ArrayList<Plan>();
									tempPlans.add(plan);
									tempPlans = backwardPlanning(services, request, tempPlans);
									for (int j = 0; j < tempPlans.size(); j++) {
										plans.add(tempPlans.get(j));
									}
								}
							} else {
								sequence.setOrder(plan.getSteps().size());
								plan.setInputs(serviceInputs);
								plan.getSteps().add(sequence);

								ArrayList<ServiceParameter> comparedInputs = compareParameters(requestInputs,
										sequence.getInputs());
								if (comparedInputs.size() == 0 || comparedInputs.size() < requestInputs.size()) {
									plan.setState(1);
									for(int j=0; j<services.get(i).getDomains().size(); j++){
										if(!domainIncluded(plan.getDomains(),services.get(i).getDomains().get(j)))
											plan.getDomains().add(services.get(i).getDomains().get(j));
									}
									plans.add(plan);
								}
								if (comparedInputs.size() == requestInputs.size()) {
									plan.setState(0);
									ArrayList<Plan> tempPlans = new ArrayList<Plan>();
									tempPlans.add(plan);
									tempPlans = backwardPlanning(services, request, tempPlans);
									for (int j = 0; j < tempPlans.size(); j++) {
										plans.add(tempPlans.get(j));
									}
								}
							}
						}
					}
					if (comparedOutputs.size() < requestOutputs.size() && comparedOutputs.size() != 0) {
						Plan plan = new Plan();
						plan.setState(currentPlans.get(x).getState());
						for (int k = 0; k < currentPlans.get(x).getInputs().size(); k++) {
							plan.getInputs().add(currentPlans.get(x).getInputs().get(k));
						}
						for (int k = 0; k < currentPlans.get(x).getOutputs().size(); k++) {
							plan.getOutputs().add(currentPlans.get(x).getOutputs().get(k));
						}

						for (int k = 0; k < currentPlans.get(x).getSteps().size(); k++) {
							plan.getSteps().add(getSteps(currentPlans.get(x).getSteps().get(k)));
						}
						for (int k = 0; k < currentPlans.get(x).getDomains().size(); k++) {
							plan.getDomains().add(currentPlans.get(x).getDomains().get(k));
						}
						
						ParallelStep parallel = new ParallelStep();
						parallel.setType("ParallelStep");
						
						if(plan.getSteps().get(plan.getSteps().size()-1).getClass()==ParallelStep.class){
							ParallelStep oldParallel = (ParallelStep) plan.getSteps().get(plan.getSteps().size()-1);
							parallel.setOrder(oldParallel.getOrder());
							for(int y=0;y<oldParallel.getInputs().size();y++){
								parallel.getInputs().add(oldParallel.getInputs().get(y));
							}
							for(int y=0;y<oldParallel.getOutputs().size();y++){
								parallel.getOutputs().add(oldParallel.getOutputs().get(y));
							}
							for(int y=0;y<oldParallel.getRemainingOutputs().size();y++){
								parallel.getRemainingOutputs().add(oldParallel.getRemainingOutputs().get(y));
							}
							for(int y=0;y<oldParallel.getSolvedOutputs().size();y++){
								parallel.getSolvedOutputs().add(oldParallel.getSolvedOutputs().get(y));
							}
							for(int y=0;y<oldParallel.getSteps().size();y++){
								parallel.getSteps().add(oldParallel.getSteps().get(y));
							}
							plan.getSteps().remove(plan.getSteps().size()-1);
						}else{
							parallel.setOrder(plan.getSteps().size());
							parallel.setInputs(serviceInputs);
							parallel.setOutputs(serviceOutputs);
						}
						
						parallel.setRemainingOutputs(comparedOutputs);
						
						for (int j = 0; j < requestOutputs.size(); j++) {
							int b = 0;
							for (int k = 0; k < comparedOutputs.size(); k++) {
								if (requestOutputs.get(j) == comparedOutputs.get(k)) {
									b = 1;
									break;
								}
							}
							if (b == 0){
								parallel.getSolvedOutputs().add(requestOutputs.get(j));
							}
						}
						
						SequentialStep sequence = new SequentialStep();
						sequence.setType("SequentialStep");
						for(int y=0; y<serviceInputs.size();y++){
							sequence.getInputs().add(serviceInputs.get(y));
							if(!parallel.getInputs().contains(serviceInputs.get(y))){
								parallel.getInputs().add(serviceInputs.get(y));
							}
						}
						for(int y=0; y<serviceOutputs.size();y++){
							sequence.getOutputs().add(serviceOutputs.get(y));
							if(!parallel.getOutputs().contains(serviceOutputs.get(y))){
								parallel.getOutputs().add(serviceOutputs.get(y));
							}
						}
						
						sequence.setOrder(parallel.getSteps().size());
						SimpleStep step = new SimpleStep();
						step.setType("SimpleStep");
						step.setInputs(serviceInputs);
						step.setOutputs(serviceOutputs);
						step.setService(services.get(i));
						sequence.getSteps().add(step);
						parallel.getSteps().add(sequence);
						plan.getSteps().add(parallel);
						plan.setInputs(serviceInputs);
						for(int j=0; j<services.get(i).getDomains().size(); j++){
							if(!domainIncluded(plan.getDomains(),services.get(i).getDomains().get(j)))
								plan.getDomains().add(services.get(i).getDomains().get(j));
						}
						ArrayList<Plan> tempPlans = new ArrayList<Plan>();
						tempPlans.add(plan);
						tempPlans = backwardPlanning(services, request, tempPlans);
						for (int j = 0; j < tempPlans.size(); j++) {
							plans.add(tempPlans.get(j));
						}
					}
				}
			}
		}
		plans = removeDuplicated(plans);
		return plans;
	}
	
	private boolean domainIncluded(ArrayList<Domain> domains, Domain domain) {
		for(int i = 0; i<domains.size();i++){
			if(domains.get(i).getName().equals(domain.getName()) && domains.get(i).getUrl().equals(domain.getUrl()))
				return true;
		}
		return false;
	}

	public ArrayList<Plan> checkDomains(ArrayList<Plan> plans, RequestDescription request) {
		ArrayList<Plan> res = new ArrayList<Plan>(); 
		for(int i=0; i<plans.size();i++){
			if(allDomainsIncluded(request.getDomains(), plans.get(i).getDomains())){
				res.add(plans.get(i));
			}
		}
		return res;
	}

	private boolean allDomainsIncluded(ArrayList<Domain> domainsRequest, ArrayList<Domain> domainsPlan) {
		for(int i = 0; i<domainsRequest.size(); i++){
			if(!domainIncluded(domainsPlan,domainsRequest.get(i)))
				return false;
		}
		return true;
	}
	
	private Step getSteps(Step step) {

		if (step.getClass() == SequentialStep.class) {
			SequentialStep oldStep = (SequentialStep) step;
			SequentialStep newStep = new SequentialStep();
			newStep.setType("SequentialStep");
			newStep.setOrder(oldStep.getOrder());
			for (int i = 0; i < oldStep.getInputs().size(); i++) {
				newStep.getInputs().add(oldStep.getInputs().get(i));
			}
			for (int i = 0; i < oldStep.getOutputs().size(); i++) {
				newStep.getOutputs().add(oldStep.getOutputs().get(i));
			}
			for (int i = 0; i < oldStep.getSteps().size(); i++) {
				newStep.getSteps().add((SimpleStep) getSteps(oldStep.getSteps().get(i)));
			}
			return newStep;

		} else if (step.getClass() == ParallelStep.class) {
			ParallelStep oldStep = (ParallelStep) step;
			ParallelStep newStep = new ParallelStep();
			newStep.setType("ParallelStep");
			newStep.setOrder(oldStep.getOrder());
			for (int i = 0; i < oldStep.getInputs().size(); i++) {
				newStep.getInputs().add(oldStep.getInputs().get(i));
			}
			for (int i = 0; i < oldStep.getOutputs().size(); i++) {
				newStep.getOutputs().add(oldStep.getOutputs().get(i));
			}
			for (int i = 0; i < oldStep.getSolvedOutputs().size(); i++) {
				newStep.getSolvedOutputs().add(oldStep.getSolvedOutputs().get(i));
			}
			for (int i = 0; i < oldStep.getRemainingOutputs().size(); i++) {
				newStep.getRemainingOutputs().add(oldStep.getRemainingOutputs().get(i));
			}
			for (int i = 0; i < oldStep.getSteps().size(); i++) {
				newStep.getSteps().add((ComposedStep) getSteps(oldStep.getSteps().get(i)));
			}
			return newStep;
		} else if (step.getClass() == SimpleStep.class) {
			SimpleStep oldStep = (SimpleStep) step;
			SimpleStep newStep = new SimpleStep();
			newStep.setType("SimpleStep");
			newStep.setOrder(oldStep.getOrder());
			newStep.setService(oldStep.getService());
			for (int i = 0; i < oldStep.getInputs().size(); i++) {
				newStep.getInputs().add(oldStep.getInputs().get(i));
			}
			for (int i = 0; i < oldStep.getOutputs().size(); i++) {
				newStep.getOutputs().add(oldStep.getOutputs().get(i));
			}
			return newStep;
		}
		else {
			return null;
		}
	}

	private ArrayList<ServiceParameter> compareParameters(ArrayList<ServiceParameter> requestOutputs,
			ArrayList<ServiceParameter> serviceOutputs) {
		ArrayList<ServiceParameter> parameters = new ArrayList<ServiceParameter>();
		if (requestOutputs.size() == 0)
			parameters.addAll(serviceOutputs);
		else if (serviceOutputs.size() == 0)
			parameters.addAll(requestOutputs);
		else {
			for (int i = 0; i < requestOutputs.size(); i++) {
				int b = 0;
				for (int j = 0; j < serviceOutputs.size(); j++) {
					if (requestOutputs.get(i).getName().equals(serviceOutputs.get(j).getName())
							&& requestOutputs.get(i).getType().equals(serviceOutputs.get(j).getType())) {
						b = 1;
						break;
					}
				}
				if (b == 0)
					parameters.add(requestOutputs.get(i));
			}
		}
		return parameters;
	}

	public ArrayList<Plan> removeDuplicated(ArrayList<Plan> plans) {
		if(plans.size()>1){
			ArrayList<Plan> newPlans = new ArrayList<Plan>();
			for(int i=0;i<plans.size();i++){
				Plan plan = plans.get(i);
				if(newPlans.size()==0)newPlans.add(plan);
				else{
					int add = 1;
					for(int j=0;j<newPlans.size();j++){
						Plan newPlan = newPlans.get(j);
						if(sameSteps(plan.getSteps(),newPlan.getSteps())){
							add = 0;
							break;
						}
					}
					if(add==1)newPlans.add(plan);
				}
			}
			return newPlans;
		}else return plans;
	}

	private boolean sameSteps(ArrayList<Step> steps, ArrayList<Step> steps2) {
		boolean res = true;
		if(steps.size()==steps2.size()){
			for(int i=0; i<steps.size();i++){
				if(!sameSteps(steps.get(i),steps2.get(i))){
					res = false;
					break;
				}
			}
		}else{
			res = false;
		}
		return res;
	}

	private boolean sameSteps(Step step, Step step2) {
		boolean res = true;
		if(step.getClass().equals(step2.getClass())){
			if(step.getClass().equals(SimpleStep.class)){
				SimpleStep sstep = (SimpleStep) step;
				SimpleStep sstep2 = (SimpleStep) step2;
				if(!sstep.getService().getName().equals(sstep2.getService().getName()))res = false;
			}
			if(step.getClass().equals(SequentialStep.class)){
				SequentialStep sstep = (SequentialStep) step;
				SequentialStep sstep2 = (SequentialStep) step2;
				if(!sameSteps(sstep.getSteps(),sstep2.getSteps()))res=false;
			}
			if(step.getClass().equals(ParallelStep.class)){
				ParallelStep pstep = (ParallelStep) step;
				ParallelStep pstep2 = (ParallelStep) step2;
				if(pstep.getSteps().size()==pstep2.getSteps().size()){
					int b = 0;
					for(int i=0; i<pstep.getSteps().size();i++){
						for(int j=0; j<pstep2.getSteps().size();j++){
							if(sameSteps(pstep.getSteps().get(i),pstep2.getSteps().get(j))){
								b = 1;
								break;
							}
						}
						if(b==0){
							res = false;
							break;
						}
					}
				}else res = false;
			}
		}else{
			res = false;
		}
		return res;
	}
}
